const EventEmitter = require("events");

const backgroundMitt = new EventEmitter();

const ipcEmit = (channel, ...args) => 
{
    backgroundMitt.emit("ipc-renderer", channel, ...args);
};

module.exports.backgroundMitt = backgroundMitt;
module.exports.ipcEmit = ipcEmit;
