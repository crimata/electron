const WebSocket = require("ws");
const { ipcMain } = require("electron");

const config = require("./config");
const { updateTray } = require("./tray");
const { backgroundMitt, ipcEmit } = require("./utils/emitter");

let connection = null;

let pingId;
let reconnectId;

function connectToPlatform(account)
{
    if (pingId) clearInterval(pingId);

    connection = new WebSocket(`${config.PLATFORM}/${account}`)

    .on("open", () => pingId = setInterval(() => connection.ping(null, true), 1000))

    .on("pong", () => setConnectionStatus(0))

    .on("error", () => {}) /** keep silent on error */

    .on("message", (payload) => backgroundMitt.emit("message", JSON.parse(payload)))

    .on("close", (_code, reason) => {

        setConnectionStatus(1);

        if (reason)
        {
            if (reason == "unauthorized")
            {
                backgroundMitt.emit("logout", reason);
            }

            return;
        }

        reconnectId = setTimeout(() => connectToPlatform(account), 500);

    });
}

function sendMessage(message)
{
    if (connection.readyState === WebSocket.OPEN) {
        connection.send(JSON.stringify(message));
    } else console.log("Failed to send message");
}

function disconnectFromPlatform()
{
   clearTimeout(reconnectId);
   if (connection.open) connection.close(1000, "logout");
}

function setConnectionStatus(status)
{
    updateTray("disconnect", status);
    ipcEmit("ws", status);
}

ipcMain.on("message", (_e, message) => sendMessage(message));

exports.connectToPlatform = connectToPlatform;
exports.sendMessage = sendMessage;
exports.disconnectFromPlatform = disconnectFromPlatform;
