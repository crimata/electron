const { app, protocol, ipcMain, nativeTheme } = require("electron");

const store = require("./utils/store");

const { initTray } = require("./tray");
const { createWin } = require("./window");
const { initAccount } = require("./account"); 
// const { terminateAudio } = require("./audio");

// Assert a light theme
nativeTheme.themeSource = "light";

app.on("ready", () =>
{
    initTray();

    createWin();

    initAccount();
});

// app.on("will-quit", terminateAudio);

// When user clicks app icon (re-open)
app.on("activate", createWin);

// Prevents app from quitting on window close event
app.on('window-all-closed', (e) => e.preventDefault());
