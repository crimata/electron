const { ipcMain } = require("electron");

const { post } = require("./api");
const store = require("./utils/store");
const { launchSession, endSession } = require("./session");
const { backgroundMitt, ipcEmit } = require("./utils/emitter");

store.set("account", process.env.ACCOUNT);

async function auth(_e, creds) 
{
    const { error, data } = await post("/auth", creds);

    if (error)
    {
        return data;
    }

    store.set("account", data);
    ipcEmit("account", data);
    launchSession(data);
};

function logout(_e, reason)
{
    store.delete("account");
    ipcEmit("account", false, reason);
    endSession();
};

function initAccount()
{
    ipcMain.handle("auth", auth);
    ipcMain.on("logout", logout);

    const account = store.get("account");
    if (account) launchSession(account);
}

backgroundMitt.on("logout", (reason) => logout(null, reason));


exports.initAccount = initAccount;
