const { app } = require("electron");

const DOMAIN = "crimata.com";

/* If PROD env variable exists */
const prod = process.env.PROD;

module.exports = {
    PLATFORM: prod ? `https://app.${DOMAIN}` : `http://localhost:5000`,
    API: prod ? `https://${DOMAIN}/api` : `http://localhost:5001`
}
