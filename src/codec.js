const { ipcMain } = require("electron");
const { encode, decode } = require("base64-arraybuffer");

ipcMain.handle("encode", (_e, arrayBuffer) => {
    return encode(arrayBuffer);
});

ipcMain.handle("decode", (_e, b64string) => {
    return decode(b64string);
});

exports.encode = encode;
exports.decode = decode;