const nodeAudio = require("@crimata/nodeaudio");
const { globalShortcut, ipcMain } = require("electron");

const { sendMessage } = require("./io");
const { updateTray } = require("./tray");
const { encode, decode } = require("./codec");
const { backgroundMitt, ipcEmit } = require("./utils/emitter");

let inputDevice;
let outputDevice;

let setWriteId;
let setStreamsId;

let autoStopId; /* keep track of recording time */

let playbackId; /* UID of the message being played */

const streamState = { rec: false, pb: false };

const /** @type {Int16Array[]} */ chunks = [];

backgroundMitt.on("data", (int16Arr) => {
    if (streamState.rec) chunks.push(int16Arr);
});

backgroundMitt.on("write", (int16Arr) => {
    clearTimeout(setWriteId);
    setWriteId = setTimeout(() => {
        setPlaybackStatus(false);
    }, 500);
});

function setStreams() 
{
    const defaultInput = nodeAudio.core.GetDefaultInputDevice();
    const defaultOutput = nodeAudio.core.GetDefaultOutputDevice();

    if (inputDevice !== defaultInput)
    {
        inputDevice = defaultInput;
        nodeAudio.core.CloseInputStream(inputDevice);
        nodeAudio.core.OpenInputStream(inputDevice);
    }

    if (outputDevice !== defaultOutput) 
    {
        outputDevice = defaultOutput;
        nodeAudio.core.CloseOutputStream(outputDevice);
        nodeAudio.core.OpenOutputStream(outputDevice);
    }
}

function startRecording()
{
    setRecordingStatus(true);

    /* 15s recording time limit */
    autoStopId = setTimeout(stopRecording, 15000);
}

function stopRecording()
{
    if (autoStopId)
    {
        clearTimeout(autoStopId);
    }

    setRecordingStatus(false);

    sendMessage({
        category: "audio",
        text: null,
        blob: encode(nodeAudio.utils.mergeChunks(chunks).buffer)
    });
    
    chunks.length = 0;
}

function initAudio()
{
    nodeAudio.core.Initialize(backgroundMitt.emit.bind(backgroundMitt));
    setStreamsId = setInterval(setStreams, 2000);

    const res = globalShortcut.register('CommandOrControl+Return', () => {
        streamState.rec ? stopRecording() : startRecording();
    });

    if (!res) throw new Error("Failed to register recording shortcut");
}

function playback(base64String, id)
{
    /* terminate any current playback */
    nodeAudio.core.CancelPlayback();
    ipcEmit("playback", playbackId, false);

    playbackId = id;
    setPlaybackStatus(true);
    nodeAudio.core.WriteToOutputStream(decode(base64String));
}

function terminateAudio()
{
    globalShortcut.unregisterAll();

    /* Only terminate PA if initialized */
    if (setStreamsId)
    {
        clearInterval(setStreamsId);
        nodeAudio.core.Terminate();
    }
}

function setRecordingStatus(status)
{
    streamState.rec = status;
    ipcEmit("record", status);
    updateTray("recording", streamState.rec);
}

function setPlaybackStatus(status)
{
    console.log("Setting playback status: ", status);

    streamState.pb = status;
    ipcEmit("playback", playbackId, status);
    updateTray("playback", status);
}

exports.initAudio = initAudio;
exports.terminateAudio = terminateAudio;
exports.playback = playback;
exports.streamState = streamState;
