const { contextBridge, ipcRenderer } = require("electron");

/** The main <-> render interface */

const validFromMainChannels = [
    "playback",
    "account", 
    "message",
    "record",
    "ws"
];

const validToMain = [
    "messenger",
    "message",
    "logout",
    "encode",
    "auth",
    "nav"
]

ipcRenderer.setMaxListeners(250);

// Expose protected methods that allow the renderer process to use
// the ipcRenderer without exposing the entire object
// TODO: Implement argument filtering for added security
contextBridge.exposeInMainWorld(
    "mainApi", {        
        invoke: async (channel, ...args) => {
            if (validToMain.includes(channel)) {
                try {
                    const res = await ipcRenderer.invoke(channel, ...args);
                    return res;
                } catch (e) {
                    console.log("IPCRenderer.invoke error");
                }
            }
        },
        send: (channel, ...args) => {
            if (validToMain.includes(channel)) {
                ipcRenderer.send(channel, ...args);
            }
        },
        on: (channel, func) => {
            if (validFromMainChannels.includes(channel)) {
                ipcRenderer.on(channel, (event, ...args) => func(...args));
            }
        },
        removeAllListeners: (channel) => {
            ipcRenderer.removeAllListeners(channel);
        }
    }
);
