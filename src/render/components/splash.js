const Splash =
{
    template: `
      <div id="splash">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="57.046"
          height="52.759"
          viewBox="0 0 57.046 52.759"
        >
          <g transform="translate(-4127 -507)">
            <g transform="translate(3949 332.206)">
              <g transform="translate(178 174.794)">
                <g transform="translate(0 27.405)">
                  <circle
                    cx="12.677"
                    cy="12.677"
                    r="12.677"
                    transform="translate(0 0)"
                    fill="#383838"
                  />
                  <circle
                    cx="12.677"
                    cy="12.677"
                    r="12.677"
                    transform="translate(31.692 0)"
                    fill="#383838"
                  />
                </g>
                <circle
                  cx="12.677"
                  cy="12.677"
                  r="12.677"
                  transform="translate(15.822 0)"
                  fill="#383838"
                />
                <path
                  d="M36.27,83.67"
                  transform="translate(-23.569 -43.588)"
                  fill="#ff0"
                />
              </g>
            </g>
          </g>
        </svg>
      </div>
    `
}

export default Splash;